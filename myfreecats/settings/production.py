from __future__ import absolute_import, unicode_literals

from .base import *

DEBUG = False

SECRET_KEY = os.getenv('DJANGO_SECRET_KEY')

# OpenShift checks the hostname in the routing stage, so only valid hostnames
# should reach us
ALLOWED_HOSTS = [ "*", ]

try:
    from .local import *
except ImportError:
    pass
